import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;


public class ButtonTest extends JFrame implements ActionListener {

	private JButton button = new JButton("HELLO");
	private JTextField txtName = new JTextField("ENTER NAME");
	
	public ButtonTest() {
		button.addActionListener(this);
		add(txtName, BorderLayout.NORTH);
		add(button, BorderLayout.SOUTH);
		setVisible(true);
		setSize(100,100);
	}
	
	public void actionPerformed(ActionEvent e) {
		System.out.println("HEllo " + txtName.getText() );
	}
	
	public static void main(String[] args) {
		new ButtonTest();
	}

}
